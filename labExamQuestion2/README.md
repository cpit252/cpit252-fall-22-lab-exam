## Problem

A developer wrote an application that can be used by IT admins to launch a virtual machine (VM) instance on the cloud. To launch an instance on the cloud, you will need to specify a set of required parameters as well as a number of additional optional parameters.

For example the following parameters are **required**:
cloudProvider, region, az, imageId, instanceClass, vpc, subnet, securityGroup, publicKey. 
The  following parameters are **optional**: ports, privateIP, placementGroup, architecture, hypervisor, storage, and tags.

- Fix the current implementation using the **builder design pattern**. Please make sure that you also fix the unit testing so tests are passing.

- Explain why the use of the builder design pattern is better than using *constructor overloading* (i.e., having more than one constructor with the same name but different set of parameters).

## Usage

Import the project into your IDE (e.g., IntelliJ IDEA or Apache NetBeans). You can also build and run the prgoram from the terminal using:


```bash
mvn compile exec:java
```

and to run the test, use:

```bash
mvn test
```
