package sa.edu.kau.fcit.cpit252;

import java.util.Arrays;

public class VirtualMachine {
    private String cloudProvider;
    private String region;
    private String az;
    private String imageId;
    private String instanceClass;
    private String vpc;
    private String subnet;
    private String securityGroup;
    private String publicIP;
    private String[] ports;
    private String privateIP;
    private String placementGroup;
    private String architecture;
    private String hypervisor;
    private String storage;
    private String[] tags;

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[] ports) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[] ports,
                          String privateIP, String placementGroup) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[] ports,
                          String privateIP, String placementGroup, String architecture) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
        this.architecture = architecture;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP,
                          String[] ports, String privateIP, String placementGroup, String architecture, String hypervisor) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
        this.architecture = architecture;
        this.hypervisor = hypervisor;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[]ports,
                          String privateIP, String placementGroup, String architecture, String hypervisor, String storage) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
        this.architecture = architecture;
        this.hypervisor = hypervisor;
        this.storage = storage;
    }

    public VirtualMachine(String cloudProvider, String region, String az, String imageId, String instanceClass,
                          String vpc, String subnet, String securityGroup, String publicIP, String[]ports, String privateIP,
                          String placementGroup, String architecture, String hypervisor, String storage, String[] tags) {
        this.cloudProvider = cloudProvider;
        this.region = region;
        this.az = az;
        this.imageId = imageId;
        this.instanceClass = instanceClass;
        this.vpc = vpc;
        this.subnet = subnet;
        this.securityGroup = securityGroup;
        this.publicIP = publicIP;
        this.ports = ports;
        this.privateIP = privateIP;
        this.placementGroup = placementGroup;
        this.architecture = architecture;
        this.hypervisor = hypervisor;
        this.storage = storage;
        this.tags = tags;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VirtualMachine{" +
                "cloudProvider='" + cloudProvider + '\'' +
                ", region='" + region + '\'' +
                ", az='" + az + '\'' +
                ", imageId='" + imageId + '\'' +
                ", instanceClass='" + instanceClass + '\'' +
                ", vpc='" + vpc + '\'' +
                ", subnet='" + subnet + '\'' +
                ", securityGroup='" + securityGroup + '\'' +
                ", publicIP='" + publicIP + '\'');
        if (ports != null) {
            sb.append(", ports=" + Arrays.toString(ports));
        }
        if (privateIP != null) {
            sb.append(", privateIP='" + privateIP + '\'');
        }
        if (placementGroup != null) {
            sb.append(", placementGroup='" + placementGroup + '\'');
        }
        if (architecture != null) {
            sb.append(", architecture='" + architecture + '\'');
        }
        if (hypervisor != null) {
            sb.append(", hypervisor='" + hypervisor + '\'');
        }
        if (storage != null) {
            sb.append(", storage='" + storage + '\'');
        }
        if (tags != null) {
            sb.append(", tags=" + Arrays.toString(tags));
        }
        sb.append("}");
        return sb.toString();
    }
}
