package sa.edu.kau.fcit.cpit252;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class AppTest 
{

    @Test
    public void shouldHaveSingleThreadPool()
    {
        MyThreadPool myThreadPool1 = new MyThreadPool();
        MyThreadPool myThreadPool2 = new MyThreadPool();
        assertEquals(myThreadPool1, myThreadPool2);
    }
}
