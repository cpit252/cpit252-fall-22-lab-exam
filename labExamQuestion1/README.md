## Problem

This project uses a fixed thread pool to execute some tasks by a fixed number of worker threads. Creating threads is always expensive, regardless of the platform your code is running on. This code uses a thread pool, but it ended up creating the thread pool two times and may repeat that multiple times.

Fix the current implementation, so it creates a thread pool once and clients can use it multiple times throughout the life of the application. Please make sure that you also fix the unit testing so tests are passing.

## Usage

Import the project into your IDE (e.g., IntelliJ IDEA or Apache NetBeans). You can also build and run the prgoram from the terminal using:


```bash
mvn compile exec:java
```

and to run the test, use:

```bash
mvn test
```
