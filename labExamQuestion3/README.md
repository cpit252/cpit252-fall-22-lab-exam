## Problem

![](./sharePhotoApp.png)

A developer wrote an image gallery application where users can share a photo gallery with other people via Bluetooth, messaging apps, email apps, file hosting services, and many other third-party apps.

The developer needs to implement this sharing feature in such a way that makes adding more sharing options easy and won't require significant changes to the existing code.

- Implement the share feature using the appropriate design pattern allowing the user to share a photo using at least four sharing options (Bluetooth, GMail, Messages, and Whatsapp). Please make sure that you also complete the main method and fix the unit testing so tests are passing.


## Usage

Import the project into your IDE (e.g., IntelliJ IDEA or Apache NetBeans). You can also build and run the prgoram from the terminal using:


```bash
mvn compile exec:java
```

and to run the test, use:

```bash
mvn test
```
