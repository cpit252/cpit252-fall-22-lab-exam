package sa.edu.kau.fcit.cpit252.gallery;

import java.util.Date;

public class Photo {
    private String name;
    private String path;
    private String dateTaken;

    public Photo(String name, String path, String dateTaken) {
        this.name = name;
        this.path = path;
        this.dateTaken = dateTaken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(String dateTaken) {
        this.dateTaken = dateTaken;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", dateTaken=" + dateTaken +
                '}';
    }
}
