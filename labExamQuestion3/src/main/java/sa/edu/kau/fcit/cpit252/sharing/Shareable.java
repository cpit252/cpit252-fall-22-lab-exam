package sa.edu.kau.fcit.cpit252.sharing;

public interface Shareable {
    void share(String path);
}
