package sa.edu.kau.fcit.cpit252;


import sa.edu.kau.fcit.cpit252.gallery.Gallery;
import sa.edu.kau.fcit.cpit252.gallery.Photo;

public class App
{
    public static void main( String[] args )
    {
        Gallery gallery = new Gallery();

        Photo photo1 = new Photo("beach", "", "2022-10-28");
        Photo photo2 = new Photo("mountain", "",  "2022-10-28");

        gallery.addPhoto(photo1);
        gallery.addPhoto(photo2);

        //TODO: Complete the main method to prompt the user to select a sharing method
        // and share the gallery with the selected method (Bluetooth, WhatsApp, Gmail, or Messages).
    }
}
