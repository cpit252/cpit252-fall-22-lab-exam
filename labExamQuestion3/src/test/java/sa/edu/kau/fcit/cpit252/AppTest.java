package sa.edu.kau.fcit.cpit252;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sa.edu.kau.fcit.cpit252.gallery.Gallery;
import sa.edu.kau.fcit.cpit252.gallery.Photo;
import sa.edu.kau.fcit.cpit252.sharing.Bluetooth;
import sa.edu.kau.fcit.cpit252.sharing.Gmail;
import sa.edu.kau.fcit.cpit252.sharing.Shareable;


public class AppTest 
{

    Gallery gallery;

    @Before
    public void init() {
        gallery = new Gallery();
        Photo photo1 = new Photo("beach", "/home/khalid/photos/beach.jpg", "2022-10-28");
        Photo photo2 = new Photo("mountain", "/home/khalid/mountain.jpg",  "2022-05-19");
        gallery.addPhoto(photo1);
        gallery.addPhoto(photo2);
    }

    @After
    public void teardown() {
        gallery.getPhotos().clear();
    }

    @Test
    public void shouldHavePhotosInGallery()
    {
        assertEquals(gallery.getPhotos().size(), 2);
    }

    @Test
    public void shouldShareGalleryOnGmail(){
        //gallery.share(new Gmail("example@gmail.com"));
    }

    @Test
    public void shouldShareGalleryOnWhatsApp(){
        //gallery.share(new WhatsApp("00966000000000"));
    }

    @Test
    public void shouldShareGalleryOnMessages(){
        //gallery.share(new Messages("00966000000000"));
    }

    @Test
    public void shouldShareGalleryOnBluetooth(){
        //gallery.share(new Bluetooth("bluetoothDeviceID"));
    }
}
