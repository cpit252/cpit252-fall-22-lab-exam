## Problem
Implement one of the following scenarios [ONLY ONE]: 

- **A)** An app (TweetWatcher) watches a keyword on Twitter (e.g., Makkah) and can notify a set of subscribers when someone posts a tweet that has that keyword. Posting a new tweet is what changes the state of the TweetWatcher, and it causes the subscribers to be notified via Twitter, WhatsApp, and Slack. Implement this scenario using the observer design pattern.

- **B)** An app allows users to convert from one file format to another. It starts by prompting the user to select a source file format (e.g., docx, markdown, html, etc.) and convert it into a target file format (e.g., pdf, html, docx, markdown, etc.). Implement this scenario using the template method design pattern.

## Usage

Import the project into your IDE (e.g., IntelliJ IDEA or Apache NetBeans). You can also build and run the prgoram from the terminal using:


```bash
mvn compile exec:java
```

and to run the test, use:

```bash
mvn test
```
